var testTime;
test("Increase Elapsed Time", function() {
	equal(increase(300), 300);
	equal(increase(500), 500);	
	equal(testTime = increase(8000), 8000);
	equal(testTime + increase(500), 8500);
});
testTime = 3661001;
test("String", function() {
	equal(timeToString(true, testTime), "1:1:1:1");
});