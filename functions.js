/*
 2/27/14
 1.1.2 The product must be able to open a particular 
 study guide at startup
 */
function openAtStartup()
{
	
}

/*
 1.1.2.1 It must be possible to identify the guide 
 in the URL of the main file
 */
function identifyInURL()
{
	
}

/*
 4.3 User must be able to stop the iteration
 function stopIteration()
{
	//not exactly sure how to stop an iteration... 
	//if a value is selected, stop iteration
	var stopIteration = false;
	
	if (var stopButton == true)
	{
	  //break the iteration
	  stopIteration = true;
	}

	return stopIteration;
}
*/

/*
 4.3.1 The stop action must reset the current 
 cart to the first card
 */
function resetCard()
{
	//if stopIteration is applied call upon reset()
	var reset = stopIteration();
	
	if (reset == true)
	{
	  //reset() is a method of the Iterator object
	  reset();
	}
	//not sure if there needs to be an ELSE since
	//it will not do anything if the iteration is not stopped...
}
