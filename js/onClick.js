/**
 * @author Laura VanDyke
 */

//author Susan Xayavongsa
function continueToSettingsHandler() {
	
	var addGuideButton = document.getElementById("continue");
	addGuideButton.addEventListener("click", showSettings, true);

}

//author Susan Xayavonsa
function continueToStudySetHandler() {
	
	var addGuideButton = document.getElementById("studythis");
	addGuideButton.addEventListener("click", showContent, true);

}

function showSettings() {
	"use strict";
	var settings;
	var home;
	
	settings = document.getElementById('Settings');
	settings.className = "dialog visible";
	
	home = document.getElementById('home');
	home.className = "dialog hidden";
}

function showContent() {
	"use strict";
	var content;
	var time;
	var settings;
	
	content = document.getElementById('content');
	content.className = "dialog visible";

	time = document.getElementById('time');
	time.className = "dialog visible";
		
	settings = document.getElementById('Settings');
	settings.className = "dialog hidden";
}

function initCont() {
	"use strict";
	var continueButton;
	
	continueButton = document.getElementById("continue");
	continueButton.click = showSettings;
}

function initStudy() {
	"use strict";
	var study;
	
	study = document.getElementById("studythis");
	study.click = showContent;
}