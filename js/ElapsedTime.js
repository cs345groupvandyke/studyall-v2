/*
 * @author Laura VanDyke
 */

var time = 0;

//constructor for ElapsedTime
function elapsedTime() {
	"use strict";
	time = 0;
};

//increases ElapsedTime
//@param is time in milliseconds
elapsedTime.prototype.increase = function(timeInMillis) {
	"use strict";
	return time + Number(timeInMillis);
};

//returns a string of hours minutes seconds and milliseconds
//@param boolean value and amount of time
//if no parameter is passed or the parameter is false returns time in milliseconds
function timeToString(value, time) {
elapsedTime.prototype.toString = function(value) {
	"use strict";
	var tF = Boolean(value);
	var milliseconds = time;
	var seconds;
	var minutes;
	var hours;
	var timeString;
	if (tF == true) {
		hours = Math.floor(milliseconds / (60*60*1000));
		milliseconds = milliseconds - hours * (60*60*1000);
		minutes = Math.floor(milliseconds / (60*1000));
		milliseconds = milliseconds - minutes (60*1000);
		seconds = Math.floor(milliseconds / 1000);
		milliseconds = milliseconds - seconds * 1000;

		timeString = hours + ":" + minutes + ":" + seconds + ":" + milliseconds;
	}
	else {
		timeString = milliseconds;
	}
	return timeString;
};


//restart iteration - bringing time back to 0
function restart() {
  var restart = document.getElementById("restart");
  if(restart.onclick) {
    timeToString();
    return;
  }

elapsedTime.prototype.modifyTimeText = function() {
	"use strict";
	
	var timeText = document.getElementById("timetext");
	var timeString = "";
	
	this.increase(1000);
	
	timeString = this.time.toString(true);
	
	timeText.update(timeString);
	
}
