/*
 * @author Susan Xayavongsa
 * This is another version of the Performace Tracker
 * - made a new file to make edits without messing up someone elses work...
 */
 
 var PerformanceTracker      		= function( difficulties, categories, totalCorrect, timeToAnswer, timeToAnswerDifficulty, timeToAnswerCategory ) 
 {
     this.difficulties       		= difficulties;
     this.categories         		= categories;
     this.totalCorrect       		= totalCorrect;
     this.timeToAnswer       		= timeToAnswer;
     this.timeToAnswerDifficulty	= timeToAnswerDifficulty;
     this.timeToAnswerCategory   	= timeToAnswerCategory;
     this.items             		= [];
 };
 
 //this assigns names to the elements in the peformanceTrack.add arrays
 
 PerformanceTracker.prototype.add = function( difficulty, category, timeToAnswer, correctness ) 
 {
     this.items.push(
     {
         difficulty:   difficulty,
         category:     category,
         timeToAnswer: timeToAnswer,
         correctness:  correctness
     });
 };
  
 //runs through the peformanceTrack.add arrays and pulls out the difficulty and correctness
 //all the other prototypes are similar to this one
 PerformanceTracker.prototype.getResultsForDifficulty = function( difficulty ) 
 {
     var correct   = 0,
         incorrect = 0;
 
 
     for (var i = 0; i < this.items.length; i++) 
     {
         if ( this.items[i].difficulty == difficulty ) 
         {
             if ( this.items[i].correctness ) correct++;
             else incorrect++;
         }
     }
 
     return [ correct, incorrect ];
 };
 
 PerformanceTracker.prototype.getResultsForCategory = function( category ) 
 {
     var correct   = 0,
         incorrect = 0;
 
     for (var i = 0; i < this.items.length; i++) 
     {
         if ( this.items[i].category == category ) 
         {
             if ( this.items[i].correctness ) correct++;
             else incorrect++;
         }
     }
 
     return [ correct, incorrect ];
 };
 
 
 PerformanceTracker.prototype.getResultsForTotalCorrect = function (totalCorrect) 
 {
     var correct   = 0;
         incorrect = 0;
 
     for (var i = 0; i < this.items.length; i++) {
         if (this.items[i].correctness ) correct++;
         else incorrect++;
     }
 
     return [ correct, incorrect ];
 };
 
 //this gets the time from the arrays
 PerformanceTracker.prototype.getResultsForTimeToAnswer = function () 
 {
     var time = [];
 
     for (var i = 0; i <this.items.length; i++) 
     {
          time.push( this.items[i].timeToAnswer );
     }
 
     return time;
 };
 
 //trys to get the times and display them by difficulty 
 
 PerformanceTracker.prototype.getResultsForTimeToAnswerDifficulty = function (difficulty) 
 {
     var timeDifficulty = [];
 
     for (var i = 0; i <this.items.length; i++) 
     {
         if ( this.items[i].difficulty == difficulty ) 
         {
             timeDifficulty.push( this.items[i].timeToAnswer );
         }
     }
 
     return timeDifficulty;
 };
 
//this gets the times and display the mean of them by difficulty
PerformanceTracker.prototype.getResultsForTimeToAnswerDiffMean = function (difficulty) {

    var timeDifficulty = this.getResultsForTimeToAnswerDifficulty(difficulty);
	var stats = new StatsLibrary(timeDifficulty);
    return stats.mean();
};

//this gets the times and display the median of them by difficulty
PerformanceTracker.prototype.getResultsForTimeToAnswerDiffMedian = function (difficulty) {

	var timeDifficulty = this.getResultsForTimeToAnswerDifficulty(difficulty);
   	var stats = new StatsLibrary(timeDifficulty);
    return stats.median();
};

//this gets the times and display the sd of them by difficulty
PerformanceTracker.prototype.getResultsForTimeToAnswerDiffSd = function (difficulty) 
{

	var timeDifficulty = this.getResultsForTimeToAnswerDifficulty(difficulty);
   	var stats = new StatsLibrary(timeDifficulty);
    return stats.sd();
};

//gets times and displays the mean of them by category
PerformanceTracker.prototype.getResultsForTimeToAnswerCatMean = function ( category ) 
{

    var timeCategory = this.getResultsForTimeToAnswerCategory(category);
	var stats = new StatsLibrary(timeCategory);
    return stats.mean();
};

//gets times and displays the median of them by category
PerformanceTracker.prototype.getResultsForTimeToAnswerCatMedian = function ( category ) 
{

    var timeCategory = this.getResultsForTimeToAnswerCategory(category);
	var stats = new StatsLibrary(timeCategory);
    return stats.median();
};

//gets times and displays the standard deviation of them by category
PerformanceTracker.prototype.getResultsForTimeToAnswerCatSd = function ( category ) {

    var timeCategory = this.getResultsForTimeToAnswerCategory(category);
	var stats = new StatsLibrary(timeCategory);
    return stats.sd();
};

//gets times and displays them by correctly or incorrectly
PerformanceTracker.prototype.getResultsForTimeToAnswerCorrectness = function ( correctness ) 
{
    var timeCorrectness= [];

    for (var i = 0; i < this.items.length; i++) {
        if ( this.items[i].correctness == correctness ) {
            timeCorrectness.push( this.items[i].timeToAnswer );
        }
    }
    return timeCorrectness;
};

//gets times and displays the mean of them by correctness
PerformanceTracker.prototype.getResultsForTimeToAnswerCorrectnessMean = function ( correctness ) 
{

   var timeCorrectness = this.getResultsForTimeToAnswerCorrectness(correctness);
	var stats = new StatsLibrary(timeCorrectness);
    return stats.mean();
};

//gets times and displays the median of them by correctness
PerformanceTracker.prototype.getResultsForTimeToAnswerCorrectnessMedian = function ( correctness ) 
{

    var timeCorrectness = this.getResultsForTimeToAnswerCorrectness(correctness);
	var stats = new StatsLibrary(timeCorrectness);
    return stats.median();
};

//gets times and displays the sd of them by correctness
PerformanceTracker.prototype.getResultsForTimeToAnswerCorrectnessSd = function ( correctness ) 
{

    var timeCorrectness = this.getResultsForTimeToAnswerCorrectness(correctness);
	var stats = new StatsLibrary(timeCorrectness);
    return stats.sd();
};

//gets times and displays them 

PerformanceTracker.prototype.getResultsForTimeToAnswerCategory = function ( category ) 
{
    var timeCategory= [];

    for (var i = 0; i <this.items.length; i++) 
    {
        if ( this.items[i].category == category ) 
        {
             timeCategory.push( this.items[i].timeToAnswer );
        }
     }

     return timeCategory;
};
 
 
 //these are the dictionary for the performance tracker
 var peformanceTrack = new PerformanceTracker( [ "a", "b", "c", "d" ], [ "knowledge", "analysis", "comprehension", "application", "synthesis", "evaluation" ], [ true, false ] );
 
 
 //this is what I assume I will be given from the app
 peformanceTrack.add( "a", "knowledge", 2563, true);
 peformanceTrack.add( "a", "analysis", 5000, true);
 peformanceTrack.add( "c", "knowledge", 6000, false);
 peformanceTrack.add( "a", "knowledge", 3000, false);
 
 
 console.log( peformanceTrack.getResultsForDifficulty( "c" ) );
 
 console.log( peformanceTrack.getResultsForDifficulty( "a" ) );
 
 console.log( peformanceTrack.getResultsForCategory( "knowledge" ) );
 
 console.log ( peformanceTrack.getResultsForTotalCorrect( true ) );
 
 console.log ( peformanceTrack.getResultsForTimeToAnswer() );
 
 console.log ( peformanceTrack.getResultsForTimeToAnswerDifficulty ( "a" ) );
 
 console.log ( peformanceTrack.getResultsForTimeToAnswerCategory( "analysis" ) );
 





 
