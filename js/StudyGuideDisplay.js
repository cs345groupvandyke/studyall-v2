/*
* @author Susan Xayavongsa
*/

//Event handlers
function setQuestionHandler() 
{
	window.addEventListener("load", setQuest, true);
}

function nextQuestionHandler () 
{
	var nextButton = document.getElementById("nextQuestion");
	nextButton.addEventListener("click", next, true);
}

function displayAnswerHandler() 
{
	var answerButton = document.getElementById("showAnswer");
	answerButton.addEventListener("click", displayAnswer, true);
}

function displayHintHandler() 
{
	var hintButton = document.getElementById("showHint");
	hintButton.addEventListener("click", displayHint, true);
}

function displayContextHandler() 
{
	var contextButton = document.getElementById("showContext");
	contextButton.addEventListener("click", displayContext, true);
}

function setQuestion() 
{
	i = 0;
	displayQuestion();
}

function displayQuestion() 
{
   "use strict";

   //Define variables
   var url, http, response, questions, question, questionDisplay, text;
   
   url = "SoftwareEngineering/SoftwareEngineering.xml";
   
   // Retrieve the XML document   
   http = new XMLHttpRequest();
   http.open("GET", url,false);
   http.send();
   response = http.responseXML;
   
   // Parse the XML document
   questions = response.getElementsByTagName("question");
   question = questions[i].childNodes[0].nodeValue;
   
   // Get the output elements in the HTML document
   questionDisplay = document.getElementById("question");

   // Display the question
   text = questionDisplay.firstChild;
   text.data = question;
}

function displayAnswer() 
{
	"use strict";

   //Define variables
   var url, http, response, answers, answer, answerDisplay, text;
   
   url = "SoftwareEngineering/SoftwareEngineering.xml";
   
   // Retrieve the XML document   
   http = new XMLHttpRequest();
   http.open("GET", url,false);
   http.send();
   response = http.responseXML;
   
   // Parse the XML document
   answers = response.getElementsByTagName("answer");
   answer  = answers[i].childNodes[0].nodeValue;

   // Get the output elements in the HTML document
   answerDisplay = document.getElementById("answer");

   // Display the  answer
   text = answerDisplay.firstChild;
   text.data = answer;
}

function displayHint() 
{
   "use strict";

   //Define variables
   var url, http, response, hints, hint, hintDisplay, text;
   
   url = "SoftwareEngineering/SoftwareEngineering.xml";
   
   // Retrieve the XML document   
   http = new XMLHttpRequest();
   http.open("GET", url,false);
   http.send();
   response = http.responseXML;
   
   // Parse the XML document
   hints = response.getElementsByTagName("hint");
   hint  = hints[i].childNodes[0].nodeValue;

   // Get the output elements in the HTML document
   hintDisplay = document.getElementById("hint");

   // Display the hint
   text = hintDisplay.firstChild;
   text.data = hint;
}

function displayContext() 
{
   "use strict";

   //Define variables
   var url, http, response, contexts, context, contextDisplay, text;
   
   url = "SoftwareEngineering/SoftwareEngineering.xml";
   
   // Retrieve the XML document   
   http = new XMLHttpRequest();
   http.open("GET", url,false);
   http.send();
   response = http.responseXML;
   
   // Parse the XML document
   contexts = response.getElementsByTagName("context");
   context  = contexts[i].childNodes[0].nodeValue;

   // Get the output elements in the HTML document
   contextDisplay = document.getElementById("context");

   // Display the context
   text = contextDisplay.firstChild;
   text.data = context;
}

function next() 
{
	i = i + 1;
	document.getElementById("hint").innerHTML = " ";
	document.getElementById("context").innerHTML = " ";
	document.getElementById("answer").innerHTML = " ";
	displayQuestion();
	displayAnswerHandler();
	displayHintHandler();
	displayContextHandler();
}