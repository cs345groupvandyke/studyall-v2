/*
 * @author Susan Xayavongsa
 * 
 * Acknowledgements: I recieved help from David Syhabandith in creating this file.
 */
 

function Iterator(array) {
	"use strict";
    var i;
    this.index = 0;
    this.array = new Array();
    for (i = 0; i < array.length; i++) {
		this.array[i] = array[i];
    }
}

//return true if the array has next value, return false otherwise. 
 
Iterator.prototype.hasNext = function() 
{
	return (this.index < this.array.length);
};


//return the next element of array
//initially returns th 0th element in the array and, on successive calls, returns 1st, 2nd, etc... elements.
Iterator.prototype.next = function() 
{
	if(hasNext()) 
	{
		alert("next is : " + this.array[this.index]);
        return (this.array[this.index++]);
    }
    else 
    {
		alert("It's already the LAST");
    }
};

// change the iteration to 0.
Iterator.prototype.reset = function() 
{
	this.index = 0;
    alert("the iteration is set to 0");
};
