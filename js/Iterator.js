/**
 * @author saint_000
 */
/**
 * @author Jacob Saintcross
 * @date 3/5/2014
 */

var array = []; 
var current = 0; 

function iterator(arr){
	
	this.array = arr;
	
}

iterator.prototype.hasNext = function(){
	"use strict";
	if (array[current + 1] != null)
	{ 
		return true;	
	}
	else return false; 

};

iterator.prototype.next = function(){
	"use strict";
	current = current + 1;
	
	return array[current]; 
	
};

iterator.prototype.reset = function(){
	"use strict";
	current = 0; 
};
