/**
 * @author Bryan Elliott
 */

var sortedAsc = [1, 2, 3, 4, 5];
var sortedDesc = [5, 4, 3, 2, 1];
var mixedValues = [91, 1, 23, 46, 56, 37];
var oneElemSet = [7];
var emptySet = [];
var smallArr = [1, 8, 24];
var largeArr = [1, 8, 24, 78, 126, 229, 333, 340, 400, 490, 579, 600, 777, 801];
var allNeg = [-91, -1, -23, -46, -56, -37];
var allZero = [0, 0, 0, 0, 0];


module("Mean Tests");
test( "Mean, Unsorted Data Set", function() {
  equal(mean(mixedValues), 42.3333);
});
test( "Mean, 1 Element Set", function() {
  equal(mean(oneElemSet), 7);
});
test( "Mean, Empty Set", function() {
  equal(mean(emptySet), 0);
});
test( "Mean, Sorted Ascending Set", function() {
  equal(mean(sortedAsc), 3);
});
test( "Mean, Sorted Descending Set", function() {
  equal(mean(sortedDesc), 3);
});
test( "Mean, All Zero Set", function() {
  equal(mean(allZero), 0);
});
test( "Mean, All Negative Set", function() {
  equal(mean(allNeg), -42.3333);
});
test( "Mean, Large Array", function() {
  equal(mean(largeArr), 341.8571);
});
test( "Mean, Small Array", function() {
  equal(mean(smallArr), 11);
});



module("Median Tests");
test( "Median, Unsorted Data Set", function() {
  equal(median(mixedValues), 41.5);
});
test( "Median, 1 Element Set", function() {
  equal(median(oneElemSet), 7);
});
test( "Mean, Empty Set", function() {
  equal(median(emptySet), 0);
});
test( "Median, Sorted Ascending Set", function() {
  equal(median(sortedAsc), 3);
});
test( "Median, Sorted Descending Set", function() {
  equal(median(sortedDesc), 3);
});
test( "Median, All Zero Set", function() {
  equal(median(allZero), 0);
});
test( "Median, All Negative Set", function() {
  equal(median(allNeg), -41.5);
});
test( "Median, Large Array", function() {
  equal(median(largeArr), 336.5);
});
test( "Median, Small Array", function() {
  equal(median(smallArr), 8);
});

module("Standard Deviation Tests");
test( "Standard Deviation, Standard Data Set", function() {
  equal(sd(mixedValues), 30.5919);
});
test( "Standard Deviation, Empty Set", function() {
  equal(sd(emptySet), 0);
});
test( "Median, Sorted Ascending Set", function() {
  equal(sd(sortedAsc), 1.5811);
});
test( "Standard Deviation, Sorted Descending Set", function() {
  equal(sd(sortedDesc), 1.5811);
});
test( "Standard Deviation, All Zero Set", function() {
  equal(sd(allZero), 0);
});
test( "Standard Deviation, All Negative Set", function() {
  equal(sd(allNeg), 30.5919);
});
test( "Standard Deviation, Large Array", function() {
  equal(sd(largeArr), 278.1911);
});
test( "Standard Deviation, Small Array", function() {
  equal(sd(smallArr), 11.7898);
});
