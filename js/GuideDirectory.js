function GuideDirectory( url )
{
	this.url      = url;
	this.request  = new XMLHttpRequest;
	
	this.request.open( "GET", url, false );
	this.request.send();
	
	this.response = this.request.responseXML;
}

GuideDirectory.prototype.getDescriptions = function() 
{
	var studyGuides, descriptions, i;

	descriptions = new Array();

	studyGuides = this.response.getElementsByTagName( "entry" );

	for( i = 0; i < studyGuides.length; i++ ) 
	{
		descriptions.push(studyGuides[ i ].getAttributeNode( "description" ).nodeValue );
	}

	return descriptions;
};

GuideDirectory.prototype.getStudyGuide = function( description ) 
{
	var studyGuides, i, toCheck;

	studyGuides = this.response.getElementsByTagName( "entry" );

	for( i = 0; i < studyGuides.length; i++ ) 
	{

		toCheck = studyGuides[ i ].getAttributeNode( "description" ).nodeValue;
		
		if( description == 
			studyGuides[ i ].getAttributeNode( "description" ).nodeValue ) 
			{

			return studyGuides[ i ].getAttributeNode( "file" ).nodeValue;
		}	
	}

	return null;
};