/*
 * @author Susan Xayavongsa
 */

//Event Handlers
function onLoadHandler() 
{
	window.addEventListener("load", cardStart, true);
}

function nextQuestionHandler() 
{
	var nextButton = document.getElementById("nextQuestion");
	nextButton.addEventListener("click", cardStop, true);
}

function showTimeHandler() 
{
	var showButton = document.getElementById("showTimes");
	showButton.addEventListener("click", showTimes, true);
}

function reportTimeHandler() 
{
	var showButton = document.getElementById("timeSpent");
	showButton.addEventListener("click", timeSpent, true);
}

function resetArrayHandler() 
{
	var resetButton = document.getElementById("reset");
	resetButton.addEventListener("click", resetArray, true);
}

var intCardStartTime = "";
var intCardStopTime  = "";
var cardTimes = [];

//Begin timer
function cardStart() 
{
	var cardStartTime = new Date().getTime();
	intCardStartTime = parseInt(cardStartTime);
}

//Stop timer
function cardStop() 
{
	var cardStopTime = new Date().getTime();
	intCardStopTime = parseInt(cardStopTime);
	cardTime();
	cardStart();
}

//Calculate time spent on card
function cardTime() 
{
	var difference = (intCardStopTime-intCardStartTime);
	cardTimes.push(difference);
}

function showTimes() 
{
	alert(cardTimes.toString());
}

/************************************/
//Reset array when iteration is reset
function resetArray() 
{
	cardTimes.length = 0;
}

function Init() 
{
    array = new Array();
    var i;
}

Init.prototype.init = function() 
{
    for (i = 0; i < 3; i++) 
    {
        array[i] = i;
    }
    var a;
    a = new Iterator(array);
    alert("the array is :" + array.toString);
};

/************************************/
//Handler for next Button 
function nextHandlers() 
{
	"use strict";
    var nextButton;
    nextButton = document.getElementById("nextQuestion");
    nextButton.onclick = Iterator.prototype.next;
}

//Handler for reset Button 
function resetHandlers() 
{
    var resetButton;
    resetButton = document.getElementById("reset");
    resetButton.onclick = Iterator.prototype.reset;
}

//Handler for start button 
function startHandlers() 
{
    window.onload = Init.prototype.init;
}

/************************************/
function createURL(urlstring) 
{
	window.location.assign(urlstring);
}

function getProtocol() 
{
	return window.location.protocol;
}

function getHost() 
{
	return window.location.host;
}

function getQueryString() 
{
	var query = window.location.search;
	var querySplit = query.split("?");
	return querySplit;
}

function getPath() 
{
	return window.location.pathname;
}

function getValue()
{

}
