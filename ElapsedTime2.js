/*
* @author Susan Xayavongsa
*/

//Event Handlers
function beginTimeHandler() 
{
	window.addEventListener("load", startTimer, true);
}

function stopButtonHandler() 
{
	var stopBtn = document.getElementById("stop");
	stopBtn.addEventListener("click", stopTimer, true);
}

//Constructer for ElapsedTime
function ElapsedTime(time) 
{
	this.time = time;
}

var startTime = "";
var stopTime = "";

//Start the timer
function startTimer() 
{
	var startTime = new Date().getTime();
	intStartTime = parseInt(startTime);
}

//Stop the timer
function stopTimer() 
{
	var stopTime = new Date().getTime();
	intStopTime = parseInt(stopTime);
	increase();
}

//Determine the total time elapsed
function increase() 
{
	var difference = (intStopTime-intStartTime);
	var hours   = pad(Math.floor(difference/3600000));
	var minutes = pad(Math.floor(difference/60000));
	var seconds = pad(Math.floor(difference/1000));
	var milliseconds = pad2((difference%1000));
	var timeFormat = hours + ":" + minutes + ":" + seconds + "." + milliseconds;
	totalTime = new ElapsedTime(timeFormat);
	document.getElementById("display").innerHTML = totalTime.time;
}

//Format hours, minutes, seconds, for proper display
function pad(number) 
{
	return (number < 10 ? '0' : '') + number;
}
			
//Format milliseconds for proper display
function pad2(number) 
{
	if(number < 10) 
	{
		return("00" + number);
	} 
	else if(number > 9 && number < 100) 
	{
		return("0" + number);
	} 
	else 
	{
		return(number);
	}
}

//Determine what should be displayed
function toString() 
{
	var booleanVar = new Boolean();
	if(booleanVar === true) 
	{
		return(timeDisplay);
	} 
	else 
	{
		return timeInMillis;
	}
}
