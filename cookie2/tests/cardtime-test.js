test("CardTime Test", function() {
	equal(1200, cardtime(3600, 2400));
	equal(1843, cardtime(1900, 57));
	equal(1360, cardtime(1400, 40));
});