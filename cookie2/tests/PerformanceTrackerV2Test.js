 //below is the tests for QUnit. They test to see if Performance Tracking is creating arrays for the functions
 module('Get Results');
     test( "getResultsForDifficultyArray", function() {
       ok ( (peformanceTrack.getResultsForDifficulty() instanceof Array), "performanceTrack.getResultsForDifficulty produces an array.");
       ok ( (peformanceTrack.getResultsForDifficulty().length), "peformanceTrack.getResultsForDifficulty has at least one item");
     });
 
     test( "getResultsForCategoryArray", function() {
       ok ( (peformanceTrack.getResultsForCategory() instanceof Array), "getResultsForCategory produces an array.");
       ok ( (peformanceTrack.getResultsForCategory().length), "getResultsForCategory has at least one item ");
     });
 
     test( "getResultsForTotalCorrectArray", function() {
       ok ( (peformanceTrack.getResultsForTotalCorrect() instanceof Array), "getResultsForTotalCorrect produces an array.");
       ok ( (peformanceTrack.getResultsForTotalCorrect().length), "getResultsForTotalCorrect has at least one item ");
     });
 
     test( "getResultsForTimeToAnswerArray", function() {
       ok ( (peformanceTrack.getResultsForTimeToAnswer() instanceof Array), "getResultsForTimeToAnswer produces an array.");
       ok ( (peformanceTrack.getResultsForTimeToAnswer().length), "getResultsForTimeToAnswer has at least one item ");
     });
 
     test( "getResultsForTimeToAnswerDiffArray", function() {
       ok ( (peformanceTrack.getResultsForTimeToAnswerDifficulty() instanceof Array), "getResultsForTimeToAnswerDifficulty produces an array.");
       ok ( (peformanceTrack.getResultsForTimeToAnswerDifficulty( "a", "b", "c", "d").length), "getResultsForTimeToAnswerDifficulty has at least one item ");
     });
 
     test( "getResultsForTimeToAnswerCatArray", function() {
       ok ( (peformanceTrack.getResultsForTimeToAnswerCategory() instanceof Array), "getResultsForTimeToAnswerCategory produces an array.");
       ok ( (peformanceTrack.getResultsForTimeToAnswerCategory( "knowledge", "analysis", "comprehension", "application", "synthesis", "evaluation" ).length), "getResultsForTimeToAnswerCategory has at least one item.");
     });
     
/*
 *Tests for mean, median, and standard deviation of times
 */

console.log ( peformanceTrack.getResultsForTimeToAnswerDiffMean( "a" ) );
console.log ( peformanceTrack.getResultsForTimeToAnswerDiffMedian( "a" ) );
console.log ( peformanceTrack.getResultsForTimeToAnswerDiffSd( "a" ) );

console.log ( peformanceTrack.getResultsForTimeToAnswerCatMean( "knowledge" ) );
console.log ( peformanceTrack.getResultsForTimeToAnswerCatMedian( "knowledge" ) );
console.log ( peformanceTrack.getResultsForTimeToAnswerCatSd( "knowledge" ) );

console.log ( peformanceTrack.getResultsForTimeToAnswerCorrectnessMean( true ) );
console.log ( peformanceTrack.getResultsForTimeToAnswerCorrectnessMedian( true ) );
console.log ( peformanceTrack.getResultsForTimeToAnswerCorrectnessSd( true ) );

console.log ( peformanceTrack.getResultsForTimeToAnswerCorrectnessMean( false ) );
console.log ( peformanceTrack.getResultsForTimeToAnswerCorrectnessMedian( false ) );
console.log ( peformanceTrack.getResultsForTimeToAnswerCorrectnessSd( false ) );