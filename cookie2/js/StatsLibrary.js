/**
 *	Statistics Library
 *  Contains functions to perform various statistical tasks.
 * 
 *  @author Bryan Elliott
 *  This work complies with the JMU Honor Code.
 */ 

/**
 * mean() - computes the average of an array of values
 * 
 * Parameter: values - an array of values that the function computes the mean
 * from
 * 
 */


function mean(values) {
	"use strict";
	
	var mean = 0;
	var sum = 0;
	var size = values.length;
	
	if (size == 0) {
		//do nothing
	}
	else if (size == 1) {
		mean = values[0];
	}
	else {
		for (var i = 0; i < size; i++) {
			if (isNaN(values[i])) {
				//do nothing
			}
			else {
				sum += values[i];
			}
		}
	
		mean = sum / size;
	}
	

	return mean.toFixed(4);
}

/**
 * median() - finds the median of an array of values
 * 
 * Parameter: values - an array of values that the function computes the median
 * from
 * 
 */

function median(values) {
	"use strict";
	
	var median = 0;
	var medIndex;
	var size = values.length;
	
	if (size == 0) {
		//do nothing
	}
	else if (size == 1) {
		median = values[0];
	}
	else{
		values.sort(function(a, b) {return a-b;});
		if ((size % 2) == 0) {
			var value1 = values[size / 2];
			var value2 = values[(size / 2) - 1];
			
			median = ((value1 + value2) / 2);
		}
		else {
			medIndex = Math.floor((size / 2));
			median = values[medIndex];
		}
	}
	
	return median.toFixed(4);
	
}

/**
 * sd() - computes the standard deviation of an array of values
 * 
 * Parameter: values - an array of values that the function computes the 
 * standard deviation from
 * 
 */

function sd(values) {
	"use strict";
	
	var average = mean(values);
	var sd;
	var size = values.length;
	var sum = 0;
	var underRadical;
		
	if (size == 0) {
		sd = 0;
	}
	else {
		for (var i = 0; i < size; i++) {
			sum += Math.pow((values[i] - average), 2);
		}
		
		underRadical = (sum/(size - 1));
		
		sd = Math.sqrt(underRadical);
	}
		
	return sd.toFixed(4);
	
}
