/**
 * @author Laura VanDyke
 * This work complies with the JMU Honor Code.
 */
var count=0;
var counter=setInterval(timer, 1000); //runs every second

function timer() {
	"use strict";
  	count=count-1;
 	 if (count <= 0) {
     	clearInterval(counter);
     return;
}

count=document.getElementById("setTime");
counter=setInterval(timer, 1000); //runs every second

function minToSec() {
	"use strict";
	count=count*60;
	return count;
}

function timer() {
	"use strict";
   	var time;
   	var timeString;
   	
	time=minToSec()-1;
	timeString=toString(time);
	
	if (time <= 0) {
		clearInterval(counter);
		return;
	}
	
  document.getElementById("setTime").innerHTML=timeString;
}

/*
 * @param time from the current iteration of timer
 * @return a string of how many remaining minutes and seconds
 */
function toString(time) {
	"use strict";
	var minutes;
	var seconds;
	var timeString;
	
	minutes = time / 60;
	seconds = Math.ceil((minutes - Math.floor(minutes)) * 60);
	minutes = Math.floor(minutes);
	
	while (seconds >= 60) {
		var temp = Math.floor(seconds / 60);
		minutes += temp;
		seconds += Math.ceil((temp- Math.floor(temp)) * 60);
	}
	
	timeString = minutes + ":" + seconds;
	return timeString;
}