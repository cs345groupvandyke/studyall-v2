/**
 * @author Jacob Saintcross
 * @date 2/24/14 
 * This work complies with the JMU Honor Code.
 */

var diffArray = [][2]; 
var catArray = [][2]; 


//The Constructor
function performanceTracker(diff, cat)
{
	"use strict";	
	this.diffArray = iterator(diff);
	this.catArray = iterator(cat); 
}

//The add() method
performanceTracker.prototype.add = function(diff, cat, time, rightOrWrong)
{
	"use strict";
	diffArray.reset();
	catArray.reset();
	
	for (var i = 0; i < diffArray.length; i++)
	{
		if (diffArray.next() == diff)
		{
			for (var j = 0; j < diffArray[i].length; j++)
			{
				if (tOrF == true)
				{
					diffArray[i][0] += 1;
				}
				if (tOrF == false)
				{
					diffArray[i][1] += 1;
				}	
			}
		}		
	}
	for (var x = 0; x < catArray.length; x++)
	{
		if (catArray.next() == cat)
		{
			for (var y = 0; y < catArray[i].length; y++)
			{
				if (tOrF == true)
				{
					catArray[i][0] += 1;
				}
				if (tOrF == false)
				{
					catArray[i][1] += 1;
				}
			}
		}
	}
	
	
	
};

//The getResultsForDifficulty() method
// Returns an array of [2] ints, number correct and incorrect
performanceTracker.prototype.getResultsForDifficulty = function(diff)
{
	"use strict";
	diffArray.reset(); 
	var rightWrongDiff = []; 
	for (var i = 0; i < diffArray.length; i++)
	{
		if (diffArray.next() == diff)
		{
			for (var j = 0; j < 2; j++)
			{
				rightWrongDiff += diffArray[i][j];	
		}
	}
	
	return rightWrongDiff;
	
};

//The getResultsForCategory() method
// Returns an array of [2] ints, number correct and incorrect
performanceTracker.prototype.getResultsForCategory = function(cat)
{
	"use strict";
	catArray.reset(); 
	var rightWrongCat = []; 
	for (var i = 0; i < catArray.length; i++)
	{
		if (catArray.next() == cat)
		{
			for (var j = 0; j < 2; j++)
			{
				rightWrongCat += catArray[i][j];	
		}
	}
	
	return rightWrongCat;
	
	
};

