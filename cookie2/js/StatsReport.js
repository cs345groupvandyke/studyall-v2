/**
 * StatsReport.js
 * 
 * @author Bryan Elliott
 * This work complies with the JMU Honor Code.
 */

	var amean;
	var bmean;
	var cmean;
	var dmean;
	var knowmean;
	var compmean;
	var appmean;
	var analysismean;
	var synthmean;
	var evalmean;
	
	var amedian;
	var bmedian;
	var cmedian;
	var dmedian;
	var knowmedian;
	var compmedian;
	var appmedian;
	var analysismedian;
	var synthmedian;
	var evalmedian;
	
	var asd;
	var bsd;
	var csd;
	var dsd;
	var knowsd;
	var compsd;
	var appsd;
	var analysissd;
	var synthsd;
	var evalsd;


StatsReport.prototype.getMeanElements = function() {
	"use strict";
	amean = document.getElementById("amean");
	bmean = document.getElementById("bmean");
	cmean = document.getElementById("cmean");
	dmean = document.getElementById("dmean");
	knowmean = document.getElementById("knowmean");
	compmean = document.getElementById("compmean");
	appmean = document.getElementById("appmean");
	analysismean = document.getElementById("analysismean");
	synthmean = document.getElementById("synthmean");
	evalmean = document.getElementById("evalmean");

};

StatsReport.prototype.getMedianElements = function() {
	"use strict";
	amedian = document.getElementById("amedian");
	bmedian = document.getElementById("bmedian");
	cmedian = document.getElementById("cmedian");
	dmedian = document.getElementById("dmedian");
	knowmedian = document.getElementById("knowmedian");
	compmedian = document.getElementById("compmedian");
	appmedian = document.getElementById("appmedian");
	analysismedian = document.getElementById("analysismedian");
	synthmedian = document.getElementById("synthmedian");
	evalmedian = document.getElementById("evalmedian");

};

StatsReport.prototype.getSdElements = function() {
	"use strict";
	asd = document.getElementById("asd");
	bsd = document.getElementById("bsd");
	csd = document.getElementById("csd");
	dsd = document.getElementById("dsd");
	knowsd = document.getElementById("knowsd");
	compsd = document.getElementById("compsd");
	appsd = document.getElementById("appsd");
	analysissd = document.getElementById("analysissd");
	synthsd = document.getElementById("synthsd");
	evalsd = document.getElementById("evalsd");

};

StatsReport.prototype.reportStats = function(ptrack) {
	
};


