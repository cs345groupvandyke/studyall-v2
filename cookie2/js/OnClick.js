/**
 * @author Laura VanDyke
 * 
 * This work complies with the JMU Honor Code.
 */

function startSEHandler() {
	"use strict";
	var addGuideButton = document.getElementById("sEngineering");
	addGuideButton.addEventListener("click", showContent, true);

}

function startHCHandler() {
	"use strict";
	var addGuideButton = document.getElementById("htmlcss");
	addGuideButton.addEventListener("click", showContent, true);

}

/*function continueToSettingsHandler() {
	"use strict";
	var addGuideButton = document.getElementById("continue");
	addGuideButton.addEventListener("click", showSettings, true);

}

function continueToStudySetHandler() {
	"use strict";
	var addGuideButton = document.getElementById("studythis");
	addGuideButton.addEventListener("click", showContent, true);

}

function showSettings() {
	"use strict";
	var settings;
	var home;
	
	settings = document.getElementById('Settings');
	settings.className = "dialog visible";
	
	home = document.getElementById('home');
	home.className = "dialog hidden";
}*/

function showContent() {
	"use strict";
	var content;
	var time;
	var settings;
	var home;
	
	
	content = document.getElementById('content');
	content.className = "dialog visible";

	time = document.getElementById('time');
	time.className = "dialog visible";
		
	//settings = document.getElementById('Settings');
	//settings.className = "dialog hidden";
}

function initCont() {
	"use strict";
	var continueButton;
	
	//continueButton = document.getElementById("continue");
	//continueButton.click = showSettings;
	
	continueButton = document.getElementById("select");
	continueButton.click = showContent;
}

function initStudy() {
	"use strict";
	var study;
	
	study = document.getElementById("select");
	study.click = showContent();
}