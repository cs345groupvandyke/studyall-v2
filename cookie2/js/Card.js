//Event Handlers
function onLoadHandler() {
	window.addEventListener("load", cardStart, true);
}

function correctButtonHandler() {
	var correctButton = document.getElementById("correct");
	correctButton.addEventListener("click", increaseRight, true);

function incorrectButtonHandler() {
	var incorrectButton = document.getElementById("incorrect");
	incorrectButton.addEventListener("click", increaseWrong, true);
}


function nextQuestionHandler() {
	var nextButton = document.getElementById("nextQuestion");
	nextButton.addEventListener("click", cardStop, true);
}

function showTimeHandler() {
	var showButton = document.getElementById("showTimes");
	showButton.addEventListener("click", showTimes, true);
}

function resetArrayHandler() {
	var resetButton = document.getElementById("reset");
	resetButton.addEventListener("click", resetArray, true);
}

var intCardStartTime = "";
var intCardStopTime  = "";
var cardTimes = [];

//Begin timer
function cardStart() {
	var cardStartTime = new Date().getTime();
	intCardStartTime = parseInt(cardStartTime);
}

//Stop timer
function cardStop() {
	var cardStopTime = new Date().getTime();
	intCardStopTime = parseInt(cardStopTime);
	cardTime();
	cardStart();
}

//Keep track of 

//Calculate time spent on card
function cardTime() {
	var difference = (intCardStopTime-intCardStartTime);
	cardTimes.push(difference);
}

function showTimes() {
	var ii;
	var time;
	var timesForCards=[];
	for (ii=0; ii<cardTimes.length; ii++){
		time=cardTimes[ii];
		timesForCards[ii]=toString(time);
	}
	alert(timesForCards.toString());
}

//Reset array when iteration is reset
function resetArray() {
	cardTimes.length = 0;
}
//toSting method
function toString(time) {
	var milliseconds = time;
	var minutes = 0;
	var seconds = 0;
	var timeString;
	while (milliseconds >= 1000) {
		var check;
		seconds = milliseconds / 1000; 
		check = seconds - Math.floor(seconds);
		if (check >= 0.5) {
			seconds = Math.ceil(seconds);
		}
		else {
			seconds = Math.floor(seconds);
		}
		milliseconds = milliseconds - (seconds * 1000);
	}
	while (seconds >= 60) {
		var temp = Math.floor(seconds / 60);
		minutes += temp;
		seconds += Math.ceil((temp - Math.floor(temp)) * 60);
	}
	timeString = minutes + ":" + seconds;
	return timeString;
}
