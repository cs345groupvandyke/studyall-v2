/**
* @author Susan Xayavongsa
* This work complies with the JMU Honor Code.
*/

function GuideDisplay() {
	

}

function addGuideHandler() {

	
	var addGuideButton = document.getElementById("submitbutton");

	"use strict";
	var addGuideButton = document.getElementById("addGuide");

	addGuideButton.addEventListener("click", addGuide, true);

}

function startGuideHandler() {
	"use strict";
	var startGuideButton = document.getElementById("select");
	startGuideButton.addEventListener("click", displayQuestion, true); 

}

function startHCGuideHandler() {
	"use strict";	
	var startGuideButton = document.getElementById("htmlcss");
	startGuideButton.addEventListener("click", setQuestion, true); 

}

function displayAnswerHandler() 
{
	"use strict";
	var answerButton = document.getElementById("showAnswer");
	answerButton.addEventListener("click", displayAnswer, true);
}

function displayHintHandler() 
{
	"use strict";
	var hintButton = document.getElementById("showHint");
	hintButton.addEventListener("click", displayHint, true);
}

function displayContextHandler() 
{
	"use strict";
	var contextButton = document.getElementById("showContext");
	contextButton.addEventListener("click", displayContext, true);
}

function nextHandler () 
{
	"use strict";
	var nextButton = document.getElementById("nextQuestion");
	nextButton.addEventListener("click", next, true);
}


function addGuide() {
	"use strict";
	// call initialization file
	if (window.File && window.FileList && window.FileReader) {
		Init();
//
// initialize
function Init() {
	"use strict";
	var fileSelect = document.getElementById("guide-file");
		
	// file select
	fileSelect.addEventListener("change", fileSelectHandler, true); 
	function fileSelectHandler() {
		var fileName = fileSelect.getFilename();
		var list = document.getElementById("guideList");

	
		radioInput = document.createElement('input');
        radioInput.setAttribute('type', 'radio');
        radioInput.setAttribute('name', fileName);
        radioInput.setAttribute('value', fileName);
        list.appendChild(radioInput);

	}

}


function displayQuestion(file) 
{
   "use strict";

   //Define variables
   var http, url, response, questions, question, questionDisplay, text;
   
  	var loader = this.getClass().getClassLoader();
	var url = loader.getResourceAsStream("guides/" + file);
   
   http = new XMLHttpRequest();
   http.open("GET", url,false);
   http.send();
   response = http.responseXML;
   // Parse the XML document
   questions = response.getElementsByTagName("question");
   question = questions[i].childNodes[0].nodeValue;
   
   // Get the output elements in the HTML document
   questionDisplay = document.getElementById("question");

   // Display the question
   text = questionDisplay.firstChild;
   text.data = question;
}

function displayAnswer() 
{
	"use strict";

   //Define variables
   var url, http, response, answers, answer, answerDisplay, text;
   
   url = "guides/SoftwareEngineering/SoftwareEngineering.xml";
   
   // Retrieve the XML document   
   http = new XMLHttpRequest();
   http.open("GET", url,false);
   http.send();
   response = http.responseXML;
   
   // Parse the XML document
   answers = response.getElementsByTagName("answer");
   answer  = answers[i].childNodes[0].nodeValue;

   // Get the output elements in the HTML document
   answerDisplay = document.getElementById("answer");

   // Display the  answer
   text = answerDisplay.firstChild;
   text.data = answer;
}

function displayHint() 
{
   "use strict";

   //Define variables
   var url, http, response, hints, hint, hintDisplay, text;
   
   url = "guides/SoftwareEngineering/SoftwareEngineering.xml";
   
   // Retrieve the XML document   
   http = new XMLHttpRequest();
   http.open("GET", url,false);
   http.send();
   response = http.responseXML;
   
   // Parse the XML document
   hints = response.getElementsByTagName("hint");
   hint  = hints[i].childNodes[0].nodeValue;

   // Get the output elements in the HTML document
	 hintDisplay = document.getElementById("hint");

   // Display the hint
	 text = hintDisplay.firstChild;
   text.data = hint;
}

function displayContext() 
{
   "use strict";

   //Define variables
   var url, http, response, contexts, context, contextDisplay, text;
   
   url = "guides/SoftwareEngineering/SoftwareEngineering.xml";
   
   // Retrieve the XML document   
   http = new XMLHttpRequest();
   http.open("GET", url,false);
   http.send();
   response = http.responseXML;
   
   // Parse the XML document
	 contexts = response.getElementsByTagName("context");
   context  = contexts[i].childNodes[0].nodeValue;

   // Get the output elements in the HTML document
   contextDisplay = document.getElementById("context");

   // Display the context
	 text = contextDisplay.firstChild;
   text.data = context;
}

function next() 
{
	"use strict";
	i = i + 1;
	document.getElementById("hint").innerHTML = " ";
	document.getElementById("context").innerHTML = " ";
	document.getElementById("answer").innerHTML = " ";
	displayQuestion();
	displayAnswerHandler();
	displayHintHandler();
	displayContextHandler();
}