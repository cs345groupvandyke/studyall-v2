/**
* @author Susan Xayavongsa
* 
* Acknowledgements: I received help from David Syhabandith in creating this file.
* This work complies with the JMU Honor Code. 
*/

//Event Handlers
function beginTimeHandler() {
	"use strict";
	var startBtn = document.getElementById("quickStart");
	startBtn.addEventListener("click", startTimer, true);
}

function stopButtonHandler() {
	"use strict";
	var stopBtn = document.getElementById("stop");
	stopBtn.addEventListener("click", stopTimer, true);
}

//Constructor for ElapsedTime
function ElapsedTime(time) {
	"use strict";
	this.time = time;
}

var startTime = "";
var stopTime = "";

//Start the timer
function startTimer() {
	"use strict";
	var startTime = new Date().getTime();
	intStartTime = parseInt(startTime);
}

//Stop the timer
function stopTimer() {
	"use strict";
	var stopTime = new Date().getTime();
	intStopTime = parseInt(stopTime);
	increase();
}

//Determine the total time elapsed
function increase() {
	"use strict";
	var difference = (intStopTime-intStartTime);
	var hours   = num(Math.floor(difference/3600000));
	var minutes = num(Math.floor(difference/60000));
	var seconds = num(Math.floor(difference/1000));
	var milliseconds = num2((difference%1000));
	var timeFormat = hours + ":" + minutes + ":" + seconds + "." + milliseconds;
	totalTime = new ElapsedTime(timeFormat);
	document.getElementById("display").innerHTML = totalTime.time;
}

//Format hours, minutes, seconds, for proper display
function num(number) {
	"use strict";
	return (number < 10 ? '0' : '') + number;
}
			
//Format milliseconds for proper display
function num2(number) {
	"use strict";
	if(number < 10) {
		return("00" + number);
	} else if(number > 9 && number < 100) {
		return("0" + number);
	} else {
		return(number);
	}
}

//Determine what should be displayed
function toString() {
	"use strict";
	var booleanVar = new Boolean();
	if(booleanVar === true) {
		return(timeDisplay);
	} else {
		return timeInMillis;
	}
}
