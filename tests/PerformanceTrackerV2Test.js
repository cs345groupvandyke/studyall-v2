 //below is the tests for QUnit. They test to see if Performance Tracking is creating arrays for the functions
 module('Get Results');
     test( "getResultsForDifficultyArray", function() {
       ok ( (PerformaceTracker.getResultsForDifficulty() instanceof Array), "PerformaceTracker.getResultsForDifficulty produces an array.");
       ok ( (PerformaceTracker.getResultsForDifficulty().length), "PeformaceTracker.getResultsForDifficulty has at least one item");
     });
 
     test( "getResultsForCategoryArray", function() {
       ok ( (PerformaceTracker.getResultsForCategory() instanceof Array), "getResultsForCategory produces an array.");
       ok ( (PerformaceTracker.getResultsForCategory().length), "getResultsForCategory has at least one item ");
     });
 
     test( "getResultsForTotalCorrectArray", function() {
       ok ( (PerformaceTracker.getResultsForTotalCorrect() instanceof Array), "getResultsForTotalCorrect produces an array.");
       ok ( (PerformaceTracker.getResultsForTotalCorrect().length), "getResultsForTotalCorrect has at least one item ");
     });
 
     test( "getResultsForTimeToAnswerArray", function() {
       ok ( (PerformaceTracker.getResultsForTimeToAnswer() instanceof Array), "getResultsForTimeToAnswer produces an array.");
       ok ( (PerformaceTracker.getResultsForTimeToAnswer().length), "getResultsForTimeToAnswer has at least one item ");
     });
 
     test( "getResultsForTimeToAnswerDiffArray", function() {
       ok ( (PerformaceTracker.getResultsForTimeToAnswerDifficulty() instanceof Array), "getResultsForTimeToAnswerDifficulty produces an array.");
       ok ( (PerformaceTracker.getResultsForTimeToAnswerDifficulty( "a", "b", "c", "d").length), "getResultsForTimeToAnswerDifficulty has at least one item ");
     });
 
     test( "getResultsForTimeToAnswerCatArray", function() {
       ok ( (PerformaceTracker.getResultsForTimeToAnswerCategory() instanceof Array), "getResultsForTimeToAnswerCategory produces an array.");
       ok ( (PerformaceTracker.getResultsForTimeToAnswerCategory( "knowledge", "analysis", "comprehension", "application", "synthesis", "evaluation" ).length), "getResultsForTimeToAnswerCategory has at least one item.");
     });
     
/*
 *Tests for mean, median, and standard deviation of times
 */

console.log ( PeformaceTracker.getResultsForTimeToAnswerDiffMean( "a" ) );
console.log ( PerformaceTracker.getResultsForTimeToAnswerDiffMedian( "a" ) );
console.log ( PerformaceTracker.getResultsForTimeToAnswerDiffSd( "a" ) );

console.log ( PerformaceTracker.getResultsForTimeToAnswerCatMean( "knowledge" ) );
console.log ( PerformaceTracker.getResultsForTimeToAnswerCatMedian( "knowledge" ) );
console.log ( PerformaceTracker.getResultsForTimeToAnswerCatSd( "knowledge" ) );

console.log ( PerformaceTracker.getResultsForTimeToAnswerCorrectnessMean( true ) );
console.log ( PerformaceTracker.getResultsForTimeToAnswerCorrectnessMedian( true ) );
console.log ( PerformaceTracker.getResultsForTimeToAnswerCorrectnessSd( true ) );

console.log ( PerformaceTracker.getResultsForTimeToAnswerCorrectnessMean( false ) );
console.log ( PerformaceTracker.getResultsForTimeToAnswerCorrectnessMedian( false ) );
console.log ( PerformaceTracker.getResultsForTimeToAnswerCorrectnessSd( false ) );