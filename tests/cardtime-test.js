test("CardTime Test", function() {
	equal(1200, card(3600, 2400));
	equal(1843, card(1900, 57));
	equal(1360, card(1400, 40));
});