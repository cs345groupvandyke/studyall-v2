test("Elapsed Time test", function() {
	equal(20, increase(40, 20));
	equal(78, increase(1000, 922));
	equal(123, increase(200, 77));
});