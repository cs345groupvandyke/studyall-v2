/**
 *
 * @Author: Chris Hanson
 * @Version: 2/24/2014 
 *
 */


var address;

//constructor
function Url(url)
{
	this.address = url; 
}

// getProtocol
url.prototype.getProtocol = function()
{
	var protocol = window.location.protocol + "://";
	
	return protocol;
};

// getHost
url.prototype.getHost = function()
{
	var host = window.location.host;
	
	return host;
};

// getPath
url.prototype.getPath = function()
{
	var path = "/" + window.location.pathname;
	
	return path;
};

// getQueryString
url.prototype.getQueryString = function()
{
	var url = window.location.toString();
	var query = url.split('?');
	
	return query;
};

// getValue
url.prototype.getValue = function(name)
{
	return value;
};