var testTime;
test("Increase Elapsed Time", function() {
	testTime = new elapsedTime();
	equal(testTime.increase(300), 300);
	equal(testTime.increase(500), 500);	
	equal(testTime.increase(8000), 8000);
});
test("String", function() {
	testTime = new elapsedTime();
	equal(testTime.toString(true), "1:1:1:1");
});