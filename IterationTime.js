/*
 * @author Susan Xayavongsa
 * functions that control the operations of the iteration
 * (Stop, Start, Reset, and show amount of time spent on card)
 */
//Event Handlers
function onLoadHandler() 
{
	window.addEventListener("load", cardStart, true);
}

function nextQuestionHandler() 
{
	var nextButton = document.getElementById("nextQuestion");
	nextButton.addEventListener("click", cardStop, true);
}

function reportTimeHandler() 
{
	var showButton = document.getElementById("timeSpent");
	showButton.addEventListener("click", timeSpent, true);
}

function resetArrayHandler() 
{
	var resetButton = document.getElementById("reset");
	resetButton.addEventListener("click", resetArray, true);
}

var intCardStartTime = "";
var intCardStopTime  = "";
var cardTimes = [];

//Begin timer
function cardStart() 
{
	var cardStartTime = new Date().getTime();
	intCardStartTime = parseInt(cardStartTime);
}

//Stop timer
function cardStop() 
{
	var cardStopTime = new Date().getTime();
	intCardStopTime = parseInt(cardStopTime);
	cardTime();
	cardStart();
}

//Calculate time spent on card
function cardTime() 
{
	var difference = (intCardStopTime-intCardStartTime);
	cardTimes.push(difference);
}

function showTimes() 
{
	alert(cardTimes.toString());
}

//Reset array when iteration is reset
function resetArray() 
{
	cardTimes.length = 0;
}