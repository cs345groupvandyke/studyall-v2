test("getProtocol test", function() {
      equal("http://", getProtocol());
});

test("getHost test", function() {
      equal("", getHost());      
});

test("getPath test", function() {
      equal("path", getPath());      
});

test("getQueryString test", function() {
      equal("query", getQueryString());      
});

test("getValue test", function() {
      equal("value", getValue());      
});

